package com.j30.collections.Array.MyArrayList.Zadania.Zadanie_5_Student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Student {
    private String nrIndeksu;
    private String imie;
    private String nazwisko;
    private Plec plec;
}
