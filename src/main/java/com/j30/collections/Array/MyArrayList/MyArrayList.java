package com.j30.collections.Array.MyArrayList;

public class MyArrayList <T> {
    private static final int INITIAL_ARRAY_SIZE = 10;
    private Object[] array;
    private int size = 0;  //rozmiar listy, na początku 0

    public MyArrayList() {
        this(INITIAL_ARRAY_SIZE);
    }

    public MyArrayList(int initialArraySize) {
        array = new Object[initialArraySize];
    }

    //todo:
    //-dodanie elementu (na koniec)
    //-listowanie elementów         - toString
    //-size                         - wywolanie size
    //-dodanie elementu (na pozycję)
    //-usuniecie eleemntu z pozycji n

    public int size() {
        return size;
    }


    //dodanie elementu (na koniec):

    public void add(T element) {
        checkSizeAndExtendIfNeeded(); //spradz rozmiar i rozszerz jesli trzeba
        array[size++] = element;      //postinkrementacja - najpierw wykona sie linia a potem inkremetnacja
    }

    //dodanie elementu na pozycję:

    public void add(int indeks, T element) {
        if (indeks < 0 || indeks > size) {
            throw new IndexOutOfBoundsException("Invalid index: " + indeks);
        }

        //metode nie dojdzie do tego elementu jesli wyjatek sie rzuci,
        //dlatego nie ma sensu  robic 'else' do poprzedniego if'a
        if (size >= array.length) { //brakuje miejsca w tablicy
            //1.stworzyc tablice 2x wieksza
            Object[] newArray = new Object[array.length * 2];
            for (int i = size; i > indeks; i--) {
                newArray[i] = array[i - 1]; //przepisuje nstepny w pole obecnego
            }
            newArray[indeks] = element;
            for (int i = 0; i < indeks; i++) {
                newArray[i] = array[i];
            }
            array = newArray;


        } else {
            for (int i = size - 1; i > indeks; i--) {
                array[i] = array[i - 1]; //przypisuje następny w pole obccnego
            }
            array[indeks] = element;
        }
        size++;
    }


    private void checkSizeAndExtendIfNeeded() {
        if (size >= array.length) {
            //1. stworzyc tablice dwa razy wieksza
            Object[] newArray = new Object[array.length * 2];

            //2. przepisujemy elementy
            for (int i = 0; i < array.length; i++) {
                newArray[i] = array[i];
            }

            array = newArray; //nie interesuje nas stara tablica a juz tylko nowa
        }
    }

    public T get(int n) {
        return (T) array[n];
    }

    /**
     * *usuń element z pozycji.
     *
     * @param indeks - pozycja z której usuwamy
     */
    public void remove(int indeks) {
        if (indeks >= 0 && indeks < size) {
            for (int i = indeks; i < size - 1; i++) {
                array[i] = array[i + 1];
            }
            array[--size] = null; //size = size -1;
        } else {
            throw new IndexOutOfBoundsException("Invalid index: " + indeks);
        }
    }


    @Override
    public String toString() {
        // lista -> 1 2 3 4
        //    ->[1, 2, 3, 4]
        // lista -> "abc" "def" "gha"
        //-> ["abc", "def", "gha"]  <<good
        //-> ["abc", "def", "gha", ]  <<bad
        StringBuilder sb = new StringBuilder("[");

        if (size > 0) {
            for (int i = 0; i < size; i++) {
                sb.append(array[i]);
                if (i != size - 1) {
                    sb.append(", ");
                }
            }
        }
        sb.append("]");

        return sb.toString();
    }
}








