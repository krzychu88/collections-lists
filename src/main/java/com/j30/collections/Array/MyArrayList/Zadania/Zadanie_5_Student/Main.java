package com.j30.collections.Array.MyArrayList.Zadania.Zadanie_5_Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main (String[]args){
        List<Student> students = new ArrayList<Student>();


        students.addAll(Arrays.asList(
                new Student ("123", "Pawel", "Gawel", Plec.MEZCZYZNA),
                new Student ("456", "marian", "atop", Plec.MEZCZYZNA),
                new Student ("45645", "jarek", "mors", Plec.MEZCZYZNA),
                new Student ("567", "grzesiek", "ghjjt", Plec.MEZCZYZNA),
                new Student ("857", "ania", "seisz", Plec.KOBIETA),
                new Student ("5433", "kasia", "ptak", Plec.KOBIETA),
                new Student ("4556", "ola", "morksa", Plec.KOBIETA)
                ));

        //A:
        System.out.println("Zadanie 5a:");
        System.out.println(students);

        //B:
        System.out.println("Zadanie 5b:");
        for (Student student: students){
            System.out.println(student);
        }

        //C:
        System.out.println("Zadanie 5c:");
        for (Student student: students){
            if(student.getPlec() == Plec.KOBIETA) {
                System.out.println(student);
            }
        }

        //D:
        System.out.println("Zadanie 5d:");
        for (Student student: students){
            if(student.getPlec() == Plec.MEZCZYZNA) {
                System.out.println(student);
            }
        }

        //E:
        System.out.println("Zadanie 5b:");
        for (Student student: students){
            System.out.println(student.getNrIndeksu());
        }

    }
}
